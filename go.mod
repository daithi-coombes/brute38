module gitlab.com/daithi-coombes/brute38-iterator

go 1.12

require (
	github.com/chigley/bip38 v0.0.0-20131110235420-d0d7267bfe04
	github.com/davecgh/go-spew v1.1.1
	github.com/docopt/docopt.go v0.0.0-20180111231733-ee0de3bc6815 // indirect
	github.com/gcash/bchutil v0.0.0-20190827185001-ee65943de2d3 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/sour-is/bip38tool v0.0.0-20140304045538-1b9c3e82f3fb // indirect
	github.com/sour-is/bitcoin v0.0.0-20180314143529-d37365313634
	github.com/sour-is/koblitz v0.0.0-20140508155619-28d92784add2 // indirect
	github.com/stretchr/testify v1.4.0
	gitlab.com/acid.sploit/gobip38 v0.0.0-20190124072646-fb34c678ae35 // indirect
	golang.org/x/crypto v0.0.0-20190829043050-9756ffdc2472
)

package main

import(
  "context"
  "fmt"
  "log"
  "os"
  "strings"
	"crypto/aes"

  "github.com/joho/godotenv"
  "gitlab.com/daithi-coombes/brute38-iterator/lib"
  "github.com/davecgh/go-spew/spew"
	"golang.org/x/crypto/scrypt"
  "github.com/sour-is/bitcoin/address"
)

type Message struct {
	Priv   *address.PrivateKey
	Bip38  string
	BipHex []byte
}

/**
 * @todo
 *  - allow multiple go routines: take the filename from cmd line
 *  - read all files from a dir
 */
func main(){

  // config
  err := godotenv.Load()
  if err != nil {
    log.Fatal("Error loading .env file main()")
  }

  // load word iterator
  ctx := context.Background()
  var c brute.Client
  var list brute.WordIterator
  if list, err = c.Words(ctx); err!=nil{
    log.Fatalf("Error loading client: %x\n", err)
  }
  spew.Dump(list, "^ WordIterator")

  // setup iterator params
  bip38Key := os.Getenv("BIP38_KEY")
  expectedAddress := strings.ToLower(os.Getenv("PUBLIC_KEY"))
  var in chan string
  var out chan *Message

  // start decrypter goroutine
  in, out = decryptUncompressed(bip38Key)

  word, _ := list.Next()
  run := true
  for run {
    fmt.Printf("testing[%d]: %s\n", word.Line, word.Value)
    word, err = list.Next()
    if err!=nil{
      log.Fatalf("Error getting next pswd: %s\n", err)
    }
    if word.Value==""{
      run = false
    }

    in <- word.Value
    _out := <- out
    public := _out.Priv.Address()

    if strings.ToLower(public)==expectedAddress{
      spew.Dump(public, "^ Public Key")
      spew.Dump(_out, "^ Private Key")
      spew.Dump("PRIVATE KEY FOUND!!!!!")
      log.Printf("\n\n%s\n", _out.Priv)
      close(in)
      close(out)
    } else {
      // spew.Dump(_out)
      fmt.Printf("tested: %s => %s\n", public, expectedAddress)
    }
  }

  spew.Dump(out, "^ out")
  close(in)
}

func decryptUncompressed(bip38 string) (in chan string, out chan *Message) {

	in = make(chan string)
	out = make(chan *Message)

	go func() {
		for i := range in {
			var err error
			msg := new(Message)

			msg.Bip38 = bip38
			msg.BipHex = address.FromBase58Raw(bip38)
			msg.Priv, err = Decrypt(bip38, i)

			if err != nil {
				log.Println(err)
				continue
			}
      out <- msg
		}
		close(out)
	}()

	return
}

// msg.Priv.Address() = public key
func decrypter(pass string) (in chan string, out chan *Message) {

	in = make(chan string)
	out = make(chan *Message)

	go func() {
		for i := range in {
			var err error
			msg := new(Message)

			msg.Bip38 = i
      fmt.Printf("i: %s\n", i)
			msg.BipHex = address.FromBase58Raw(i)
      fmt.Printf("BipHex: %s\n", msg.BipHex)

			msg.Priv, err = Decrypt("6PnZAt1n5qyAVNA867i5SEJ332Cni6Th5hkLGGVMXEgGd8xwJoWFfoy54X", "woopwoop")
      fmt.Printf("Priv: %s\n", msg.Priv)

			if err != nil {
				log.Println(err)
				continue
			}
      out <- msg
		}
		close(out)
	}()

	return
}

type BIP38Key struct {
	Flag byte
	Hash [4]byte
	Data [32]byte
}

func Decrypt(b38 string, passphrase string) (priv *address.PrivateKey, err error) {
	b, err := address.FromBase58(b38)
	if err != nil {
		return nil, err
	}
	_bip38 := new(BIP38Key)

	_bip38.Flag = b[2]
	copy(_bip38.Hash[:], b[3:7])
	copy(_bip38.Data[:], b[7:])

	dh, _ := scrypt.Key([]byte(passphrase), _bip38.Hash[:], 16384, 8, 8, 64)
	priv = new(address.PrivateKey)

	p := decrypt(_bip38.Data[:], dh[:32], dh[32:])
	priv.SetBytes(p)

	return
}

func decrypt(src, dh1, dh2 []byte) (dst []byte) {
	c, _ := aes.NewCipher(dh2)

	dst = make([]byte, 48)
	c.Decrypt(dst, src[:16])
	c.Decrypt(dst[16:], src[16:])
	dst = dst[:32]

	for i := range dst {
		dst[i] ^= dh1[i]
	}

	return
}

package brute

import (
  // "context"
//  "os"
  "reflect"
  "testing"
  "time"

  "github.com/stretchr/testify/assert"
//  "github.com/davecgh/go-spew/spew"
)

func TestClientWords(t *testing.T){

  ctx := context.Background()
  var c = Client{}

  actual, _ := c.Words(ctx)
  var expected WordIterator

  assert.Equal(t, reflect.TypeOf(expected), reflect.TypeOf(actual), "Wrong type returned")
}

func TestGetWord(t *testing.T){

  it, _ := getIterator()
  actual1, _ := it.Next()
  actual2, _ := it.Next()
  expected1 := Word{"seandearg", 1}
  expected2 := Word{"at", 2}

  assert.Equal(t, expected1, actual1)
  assert.Equal(t, expected2, actual2)
}

func getIterator() (WordIterator, error){
  ctx := context.Background()
  var c = Client{}

  return c.Words(ctx)
}

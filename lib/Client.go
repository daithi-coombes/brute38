package brute

import (
  "context"
)

// Client
type Client struct {}
type IClient interface {
  Words(ctx context.Context) (WordIterator)
}

// get the words iterator, initiates file pointer
func (c *Client) Words (ctx context.Context) (WordIterator, error) {

  var words WordIterator
  words.loadScanner()

  return words, nil
}

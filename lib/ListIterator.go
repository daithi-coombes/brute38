package brute

import(
  "bufio"
  "log"
  "os"
)

var scanner *bufio.Scanner
var line = 1

type Word struct {
  Value string
  Line int
}

type WordIterator struct {}
type IWordIterator interface {
  Next()
  loadScanner()
}

// get next word
func (it *WordIterator) Next() (Word, error){

  scanner.Scan()
  word := Word{Value: scanner.Text(), Line: line}
  line++

  return word, nil
}

// setup word file pointer, self::scanner *bufio.Scanner
func (it *WordIterator) loadScanner() (){

  var filename string
  args := os.Args[1:]
  if filename = args[0]; filename==""{
    log.Fatal("No filename provided")
  }

  file, err := os.Open("./data/"+filename)
	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}

	scanner = bufio.NewScanner(file)
}
